<?php

declare(strict_types=1);

namespace App\Model;

use Nette;


class AstronautRepository
{
	use Nette\SmartObject;

	const TABLE_NAME = 'astronauts';

	const ITEM_NOT_FOUND = 1;


	/** @var Nette\Database\Context */
	private $database;

	/** @var int */
	public $id;

	/** @var string */
	public $name;

	/** @var string */
	public $surname;

	/** @var string */
	public $birth_date;

	/** @var string */
	public $superpower;

	/** @var int */
	public $created;


	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
		$return = $this->database->table(self::TABLE_NAME)->insert([
			'id' => NULL,
		]);
		dump($return);
		die('wtf');
	}


	public function findAll() : Nette\Database\Table\Selection
	{
		return $this->database->table(self::TABLE_NAME);
	}


	public function findById(int $id) : Nette\Database\Table\ActiveRow
	{
		$item = $this->findAll()->get($id);
		if ($item) {
			return $item;
		} else {
			throw new \Exception('Požadovaný záznam #' . $id . ' nenalezen', self::ITEM_NOT_FOUND);
		}
	}


	public function insert(Nette\Utils\ArrayHash $values) : Nette\Database\Table\ActiveRow
	{
		$values->created = new Nette\Utils\DateTime;
		return $this->findAll()->insert($values);
	}


	public function update(int $id, Nette\Utils\ArrayHash $values) : bool
	{
		return $this->findById($id)->update($values);
	}


	public function delete(int $id) : int
	{
		return $this->findById($id)->delete();
	}

}
