<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Model\AstronautRepository;
use Nette\Application\UI\Form;


class AstronautPresenter extends Nette\Application\UI\Presenter
{

	/** @var AstronautRepository */
	private $astronauts;


	public function __construct(AstronautRepository $astronauts)
	{
		$this->astronauts = $astronauts;
	}


	protected function startup()
	{
		parent::startup();


	}


	public function renderDefault()
	{
		$this->template->items = $this->astronauts->findAll()->order('name')->order('surname')->order('birth_date');
	}


	public function renderAdd()
	{
		$this['astronautForm']['save']->caption = 'Přidat kosmonauta';
	}


	public function renderEdit(int $id = 0)
	{
		$form = $this['astronautForm'];
		if (!$form->isSubmitted()) {
			$item = $this->astronauts->findById($id);
			if (!$item) {
				$this->error('Záznam nenalezen');
			}
			$form->setDefaults($item);
		}
	}


	public function renderDelete(int $id = 0)
	{
		$this->template->item = $this->astronauts->findById($id);
		if (!$this->template->item) {
			$this->error('Záznam nenalezen');
		}
	}


	/**
	 * Edit form factory.
	 * @todo : move to custom component
	 */
	protected function createComponentAstronautForm() : Form
	{
		$form = new Form;
		$form->addText('name', 'Jméno:')
			->setAttribute('maxlength', 80)
			->setRequired('Prosím vyplňte jméno');

		$form->addText('surname', 'Příjmení:')
			->setAttribute('maxlength', 80)
			->setRequired('Prosím vyplňte i příjmení.');

		$form->addText('birth_date', 'Datum narození:')
			->setType('date')
			->setAttribute('pattern', '(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))')
			->setRequired('Prosím vyplňte i datum narození.');

		$form->addText('superpower', 'Superschpnost:')
			->setAttribute('maxlength', 255);

		$form->addSubmit('save', 'Uložit změny') // uložit změny je defaultní text a v případě vložení se přepisuje na více odpovídající
			->setHtmlAttribute('class', 'success');

		$form->addProtection();

		$form->onSuccess[] = function (Form $form, \stdClass $values) {
			$id = (int) $this->getParameter('id');
			if ($id) {
				$this->astronauts->update($id, $values);
				$this->flashMessage('Záznam kosmonauta byl upraven');
			} else {
				$this->astronauts->insert($values);
				$this->flashMessage('Záznam kosmonauta byl přidán');
			}
			$this->redirect('default');
		};

		return $form;
	}


	protected function createComponentDeleteForm() : Form
	{
		$form = new Form;
		$form->addSubmit('cancel', 'Zrušit')
			->setHtmlAttribute('class', 'storno')
			->onClick[] = function() {
				$this->flashMessage('Mazání záznamu zrušeno.');
				$this->redirect('default');
			};

		$form->addSubmit('delete', 'Odstranit')
			->setHtmlAttribute('class', 'success')
			->onClick[] = function() {
				$id = (int) $this->getParameter('id');
				$this->astronauts->delete($id);
				$this->flashMessage('Záznam kosmonauta byl smazán.');
				$this->redirect('default');
			};

		$form->addProtection();
		return $form;
	}

}
